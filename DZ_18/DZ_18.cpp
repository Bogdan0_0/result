#include <iostream>
#include <vector>
#include < string>

using namespace std;
template <typename U>

class Stack
{

public:
	void push(U);
	U pop();
	void show();

private:
	vector<U> v;

};

int main()
{
	Stack<int> a;
	a.push(12); a.push(42); a.push(55);
	a.show();
	cout << "poped:" << a.pop() << endl;
	a.show();
	return 0;
}

template<class U> void Stack<U>::push(U elem)
{
	v.push_back(elem);
}

template<class U> U Stack<U>::pop()
{
	U elem = v.back();
	v.pop_back();
	return elem;
}

template<class U> void Stack<U>::show()
{
	cout << "stack:";
	for (auto e : v) cout << e << " ";
	cout << endl;
}